$('#luxlab-carousel').on('slide.bs.carousel', function (event) {
  if ( event.direction === 'left' ) {
    $('#luxlab-carousel-content').carousel('next');
  } else if ( event.direction === 'right' ) {
    $('#luxlab-carousel-content').carousel('prev');
  }
});
